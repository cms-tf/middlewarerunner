<?php

    namespace CmsTf\MiddlewareRunner;

    use Interop\Http\ServerMiddleware\DelegateInterface;
    use Interop\Http\ServerMiddleware\MiddlewareInterface;
    use Psr\Http\Message\RequestInterface;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;

    /**
     * Class Runner
     *
     * @package CmsTf\MiddlewareRunner
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class Runner implements DelegateInterface {
        /**
         * @var ResponseInterface
         */
        protected $response;

        /**
         * The current middleware.
         *
         * @var int
         */
        protected $current = 0;

        /**
         * The registered middlewares.
         *
         * @var MiddlewareInterface[]
         */
        protected $stack = [];

        /**
         * @return ResponseInterface
         */
        public function getResponse() {
            return $this->response;
        }

        /**
         * @param ResponseInterface $response
         */
        public function setResponse($response) {
            $this->response = $response;
        }

        /**
         * Reset the counter.
         */
        public function reset() {
            $this->current = 0;
        }

        /**
         * @return MiddlewareInterface[]
         */
        public function getStack() {
            return $this->stack;
        }

        /**
         * Prepend the given middleware to the stack.
         *
         * @param MiddlewareInterface $middleware
         */
        public function prepend(MiddlewareInterface $middleware) {
            array_unshift($this->stack, $middleware);
        }

        /**
         * Append the given middleware to the stack.
         *
         * @param MiddlewareInterface $middleware
         */
        public function append(MiddlewareInterface $middleware) {
            $this->stack[] = $middleware;
        }

        /**
         * Run the current middleware.
         *
         * @param RequestInterface $request
         *
         * @return mixed
         */
        protected function runMiddleware($request) {
            $middleware = $this->stack[$this->current++];

            return call_user_func([$middleware, 'process'], $request, $this);
        }

        /**
         * Dispatch the next available middleware and return the response.
         *
         * @param ServerRequestInterface $request
         *
         * @return ResponseInterface
         */
        public function process(ServerRequestInterface $request) {
            if($this->response === null) {
                throw new \RuntimeException('Could not start the runner without an default response.');
            }

            if (!isset($this->stack[$this->current])) {
                return $this->response;
            }

            $result = $this->runMiddleware($request);

            if (!(is_object($result) && $result instanceof ResponseInterface)) {
                throw new \RuntimeException('Unknown result from the middleware.');
            }

            return $result;
        }
    }