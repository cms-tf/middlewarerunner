<?php

    namespace CmsTf\MiddlewareRunner\Tests;
    use Interop\Http\ServerMiddleware\DelegateInterface;
    use Interop\Http\ServerMiddleware\MiddlewareInterface;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;

    /**
     * Class AppendContentMiddleware
     *
     * @package CmsTf\MiddlewareRunner\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class AppendContentMiddleware implements MiddlewareInterface {

        protected $content;

        /**
         * AppendContentMiddleware constructor.
         *
         * @param $content
         */
        public function __construct($content) {
            $this->content = $content;
        }

        /**
         * Process an incoming server request and return a response, optionally delegating
         * to the next middleware component to create the response.
         *
         * @param ServerRequestInterface $request
         * @param DelegateInterface      $delegate
         *
         * @return ResponseInterface
         */
        public function process(ServerRequestInterface $request, DelegateInterface $delegate) {
            $response = $delegate->process($request);

            $response->getBody()->write($this->content);

            return $response;
        }
    }