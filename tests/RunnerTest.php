<?php

    namespace CmsTf\MiddlewareRunner\Tests;

    use GuzzleHttp\Psr7\Response;
    use GuzzleHttp\Psr7\ServerRequest;
    use PHPUnit\Framework\TestCase;
    use CmsTf\MiddlewareRunner\Runner;

    /**
     * Class TestRunner
     *
     * @package CmsTf\MiddlewareRunner\Tests
     * @author  Gerard Smit <gsmit1996@gmail.com>
     */
    class RunnerTest extends TestCase {

        public function testAppend() {
            $runner = new Runner();
            $runner->setResponse(new Response());
            $runner->append(new AppendContentMiddleware('1'));
            $runner->append(new AppendContentMiddleware('2'));
            $response = $runner->process(new ServerRequest('GET', '/'));

            self::assertEquals('21', (string)$response->getBody());
        }

    }